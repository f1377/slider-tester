# Slider Tester [2021]

Testing Sleek and Syncfusion Slider dependencies.

## Dependencies
- [sleek_circular_slider](https://pub.dev/packages/sleek_circular_slider)
- [syncfusion_flutter_sliders](https://pub.dev/packages/syncfusion_flutter_sliders)
    - [documentation](https://help.syncfusion.com/flutter/slider/overview)
- [intl](https://pub.dev/packages/intl)

## App Overview

### Sleek Single Slider Test Screen

![Sleek Single Test Screen](/images/readme/sleek_single_slider_tests_screen.png "Sleek Single Slider Test Screen")

### Sleek Multiple Slider Tests Screen

![Sleek Multiple Tests Screen](/images/readme/sleek_multiple_slider_tests_screen.png "Sleek Multiple Slider Tests Screen")

### Syncfusion Single Slider Test Screen

![Syncfusion Single Test Screen](/images/readme/syncfusion_single_slider_tests_screen.png "Syncfusion Single Slider Test Screen")

### Syncfusion Multiple Slider Tests Screen

![Syncfusion Single Tests Screen](/images/readme/syncfusion_multiple_slider_tests_screen.png "Syncfusion Multiple Slider Tests Screen")
