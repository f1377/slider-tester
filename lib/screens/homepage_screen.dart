import 'package:flutter/material.dart';
import 'package:slider_test/widgets/sleek_circular_slider_widget.dart';
import 'package:slider_test/widgets/sleek_circular_units_slider_widget.dart';
import 'package:slider_test/widgets/syncfusion_slider_widget.dart';
import 'package:slider_test/widgets/syncfusion_units_slider_widget.dart';

class HomePageScreen extends StatefulWidget {
  const HomePageScreen({Key? key}) : super(key: key);

  @override
  _HomePageScreenState createState() => _HomePageScreenState();
}

class _HomePageScreenState extends State<HomePageScreen> {
  late List<Widget> _tabBarViewItems;

  @override
  void initState() {
    _tabBarViewItems = [
      const SleekCircularUnitsSliderWidget(),
      const SleekCircularSliderWidget(),
      const SyncfusionUnitsSliderWidget(),
      const SyncfusionSliderWidget(),
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: _tabBarViewItems.length,
      child: Scaffold(
        appBar: AppBar(
          title: const Center(child: Text("Slider Tester")),
          bottom: TabBar(
            tabs: [
              Tab(
                child: Container(
                  height: 30.0,
                  width: 30.0,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.white,
                      width: 1.5,
                    ),
                    shape: BoxShape.circle,
                  ),
                  child: const Align(
                    alignment: Alignment.center,
                    child: Text('1'),
                  ),
                ),
              ),
              Tab(
                child: Container(
                  height: 30.0,
                  width: 30.0,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.white,
                      width: 1.5,
                    ),
                    shape: BoxShape.circle,
                  ),
                  child: const Align(
                    alignment: Alignment.center,
                    child: Text('2'),
                  ),
                ),
              ),
              Tab(
                child: Container(
                  height: 30.0,
                  width: 30.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(
                      color: Colors.white,
                      width: 1.5,
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text(
                        '1',
                        style: TextStyle(fontSize: 12),
                      ),
                      Icon(
                        Icons.linear_scale_sharp,
                        size: 12,
                      ),
                    ],
                  ),
                ),
              ),
              Tab(
                child: Container(
                  height: 30.0,
                  width: 30.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(
                      color: Colors.white,
                      width: 1.5,
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text(
                        '2',
                        style: TextStyle(fontSize: 12),
                      ),
                      Icon(
                        Icons.linear_scale_sharp,
                        size: 12,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: _tabBarViewItems,
        ),
      ),
    );
  }
}
