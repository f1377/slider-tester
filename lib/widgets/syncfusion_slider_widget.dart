import 'package:flutter/material.dart';
import 'package:intl/intl.dart' as intl;
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';

class SyncfusionSliderWidget extends StatefulWidget {
  const SyncfusionSliderWidget({Key? key}) : super(key: key);

  @override
  _SyncfusionSliderWidgetState createState() => _SyncfusionSliderWidgetState();
}

class _SyncfusionSliderWidgetState extends State<SyncfusionSliderWidget> {
  final double _min = 0.0;
  final double _max = 100.0;
  double _sliderValue = 40.0;

  final double _trackMin = 0.0;
  final double _trackMax = 10;
  double _trackValue = 4.0;

  final double _trickMin = 0.0;
  final double _trickMax = 10.0;
  double _trickValue = 3.0;

  final DateTime _dateMin = DateTime(2010, 01, 01);
  final DateTime _dateMax = DateTime(2022, 01, 01);
  DateTime _dateValue = DateTime(2014, 01, 01);

  final double _moneyMin = 0.0;
  final double _moneyMax = 10.0;
  double _moneyValue = 6.0;

  final DateTime _timeMin = DateTime(2000, 01, 01, 02, 00, 00);
  final DateTime _timeMax = DateTime(2000, 01, 01, 22, 00, 00);
  DateTime _timeValue = DateTime(2000, 01, 01, 12, 00, 00);

  final double _tooltipMin = 0.0;
  final double _tooltipMax = 10.0;
  double _tooltipValue = 4.0;

  final double _thumbMin = 0.0;
  final double _thumbMax = 10.0;
  double _thumbValue = 2.0;

  final double _shapeMin = 0.0;
  final double _shapeMax = 10.0;
  double _shapeValue = 4.0;

  final double _dividerMin = 0.0;
  final double _dividerMax = 10.0;
  double _dividerValue = 4.0;

  @override
  void initState() {
    super.initState();
  }

  // docs: https://help.syncfusion.com/flutter/slider/overview
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.only(left: 4.0, right: 4.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Center(
            child: SfSlider(
              min: _min,
              max: _max,
              value: _sliderValue,
              interval: 20,
              showLabels: true,
              onChanged: (dynamic newValue) {
                setState(() {
                  _sliderValue = newValue;
                });
              },
            ),
          ),
          Center(
            child: SfSliderTheme(
              data: SfSliderThemeData(
                activeTrackHeight: 10.0,
                inactiveTrackHeight: 4.0,
                trackCornerRadius: 5.0,
              ),
              child: SfSlider(
                min: _trackMin,
                max: _trackMax,
                value: _trackValue,
                interval: 2,
                activeColor: Colors.red,
                inactiveColor: Colors.red.shade100,
                showDividers: true,
                showLabels: true,
                onChanged: (dynamic newValue) {
                  setState(() {
                    _trackValue = newValue;
                  });
                },
              ),
            ),
          ),
          Center(
            child: Center(
              child: SfSliderTheme(
                data: SfSliderThemeData(
                  tickOffset: const Offset(0.0, 10.0),
                  tickSize: const Size(3.0, 12.0),
                  minorTickSize: const Size(3.0, 8.0),
                  activeMinorTickColor: Colors.red,
                  inactiveMinorTickColor: Colors.red.shade900,
                ),
                child: SfSlider(
                  min: _trickMin,
                  max: _trickMax,
                  value: _trickValue,
                  interval: 2,
                  minorTicksPerInterval: 1,
                  activeColor: Colors.green,
                  inactiveColor: Colors.green.shade100,
                  showDividers: true,
                  showTicks: true,
                  showLabels: true,
                  onChanged: (dynamic newValue) {
                    setState(() {
                      _trickValue = newValue;
                    });
                  },
                ),
              ),
            ),
          ),
          Center(
            child: Center(
              child: SfSlider(
                min: _dateMin,
                max: _dateMax,
                value: _dateValue,
                interval: 2,
                stepDuration: const SliderStepDuration(years: 2),
                dateFormat: intl.DateFormat.y(),
                dateIntervalType: DateIntervalType.years,
                // minorTicksPerInterval: 1,
                activeColor: Colors.brown,
                inactiveColor: Colors.brown.shade100,
                showDividers: true,
                showTicks: true,
                showLabels: true,
                onChanged: (dynamic newValue) {
                  setState(() {
                    _dateValue = newValue;
                  });
                },
              ),
            ),
          ),
          Center(
            child: Center(
              child: SfSlider(
                min: _moneyMin,
                max: _moneyMax,
                value: _moneyValue,
                interval: 2,
                numberFormat: intl.NumberFormat("#€"),
                minorTicksPerInterval: 1,
                activeColor: Colors.blueGrey,
                inactiveColor: Colors.blueGrey.shade100,
                showDividers: true,
                showTicks: true,
                showLabels: true,
                onChanged: (dynamic newValue) {
                  setState(() {
                    _moneyValue = newValue;
                  });
                },
              ),
            ),
          ),
          Center(
            child: Center(
              child: SfSlider(
                min: _timeMin,
                max: _timeMax,
                value: _timeValue,
                interval: 5,
                dateFormat: intl.DateFormat('h a'),
                dateIntervalType: DateIntervalType.hours,
                activeColor: Colors.teal,
                inactiveColor: Colors.teal.shade100,
                showDividers: true,
                showTicks: true,
                showLabels: true,
                onChanged: (dynamic newValue) {
                  setState(() {
                    _timeValue = newValue;
                  });
                },
              ),
            ),
          ),
          Center(
            child: Center(
              child: SfSliderTheme(
                data: SfSliderThemeData(
                  tooltipBackgroundColor: Colors.indigo,
                  tooltipTextStyle: const TextStyle(
                    color: Colors.white70,
                    fontSize: 23.0,
                    fontStyle: FontStyle.italic,
                  ),
                ),
                child: SfSlider(
                  min: _tooltipMin,
                  max: _tooltipMax,
                  value: _tooltipValue,
                  interval: 2,
                  activeColor: Colors.orange,
                  inactiveColor: Colors.orange.shade100,
                  enableTooltip: true,
                  tooltipShape: const SfPaddleTooltipShape(),
                  showDividers: true,
                  showTicks: true,
                  showLabels: true,
                  onChanged: (dynamic newValue) {
                    setState(() {
                      _tooltipValue = newValue;
                    });
                  },
                ),
              ),
            ),
          ),
          Center(
            child: Center(
              child: SfSliderTheme(
                data: SfSliderThemeData(
                  thumbColor: Colors.purpleAccent,
                  thumbRadius: 15.0,
                  thumbStrokeColor: Colors.black54,
                  thumbStrokeWidth: 2.0,
                ),
                child: SfSlider(
                  min: _thumbMin,
                  max: _thumbMax,
                  value: _thumbValue,
                  interval: 2,
                  thumbIcon: const Icon(
                    Icons.forward,
                    color: Colors.yellowAccent,
                    size: 20.0,
                  ),
                  // activeColor: Colors.deepPurpleAccent,
                  inactiveColor: Colors.deepPurpleAccent.shade100,
                  showDividers: true,
                  showTicks: true,
                  showLabels: true,
                  onChanged: (dynamic newValue) {
                    setState(() {
                      _thumbValue = newValue;
                    });
                  },
                ),
              ),
            ),
          ),
          Center(
            child: Padding(
              padding:
                  const EdgeInsets.only(top: 15.0, left: 14.0, right: 14.0),
              child: SfSliderTheme(
                data: SfSliderThemeData(
                  overlayRadius: 0,
                ),
                child: SfSlider(
                  min: _shapeMin,
                  max: _shapeMax,
                  value: _shapeValue,
                  interval: 2,
                  thumbShape: _SfTriangleThumbShape(),
                  showLabels: true,
                  showTicks: true,
                  showDividers: true,
                  onChanged: (dynamic newValue) {
                    setState(() {
                      _shapeValue = newValue;
                    });
                  },
                ),
              ),
            ),
          ),
          Center(
            child: SfSlider(
              min: _dividerMin,
              max: _dividerMax,
              value: _dividerValue,
              interval: 1,
              dividerShape: _DividerShape(),
              showLabels: true,
              showDividers: true,
              onChanged: (dynamic newValue) {
                setState(() {
                  _dividerValue = newValue;
                });
              },
            ),
          )
        ],
      ),
    );
  }
}

class _SfTriangleThumbShape extends SfThumbShape {
  @override
  void paint(
    PaintingContext context,
    Offset center, {
    required RenderBox? child,
    dynamic currentValue,
    SfRangeValues? currentValues,
    required Animation<double> enableAnimation,
    required Paint? paint,
    required RenderBox parentBox,
    required TextDirection textDirection,
    required SfSliderThemeData themeData,
    required SfThumb? thumb,
  }) {
    final Path path = Path();

    path.moveTo(center.dx, center.dy);
    path.lineTo(center.dx + 10, center.dy - 15);
    path.lineTo(center.dx - 10, center.dy - 15);
    path.close();
    context.canvas.drawPath(
        path,
        Paint()
          ..color = themeData.activeTrackColor!
          ..style = PaintingStyle.fill
          ..strokeWidth = 2);
  }
}

class _DividerShape extends SfDividerShape {
  @override
  void paint(
    PaintingContext context,
    Offset center,
    Offset? thumbCenter,
    Offset? startThumbCenter,
    Offset? endThumbCenter, {
    dynamic currentValue,
    SfRangeValues? currentValues,
    required Animation<double> enableAnimation,
    required Paint? paint,
    required RenderBox parentBox,
    required TextDirection textDirection,
    required SfSliderThemeData themeData,
  }) {
    bool isActive;

    switch (textDirection) {
      case TextDirection.ltr:
        isActive = center.dx <= thumbCenter!.dx;
        break;
      case TextDirection.rtl:
        isActive = center.dx >= thumbCenter!.dx;
        break;
    }

    context.canvas.drawRect(
        Rect.fromCenter(center: center, width: 5.0, height: 10.0),
        Paint()
          ..isAntiAlias = true
          ..style = PaintingStyle.fill
          ..color = isActive ? themeData.activeTrackColor! : Colors.white);
  }
}
