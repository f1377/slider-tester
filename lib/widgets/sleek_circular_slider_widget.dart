import 'package:flutter/material.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

class SleekCircularSliderWidget extends StatefulWidget {
  const SleekCircularSliderWidget({Key? key}) : super(key: key);

  @override
  _SleekCircularSliderWidgetState createState() =>
      _SleekCircularSliderWidgetState();
}

class _SleekCircularSliderWidgetState extends State<SleekCircularSliderWidget> {
  final double _min = 0;
  final double _max = 100;
  double _sliderValue = 40;

  final double _calMin = 0;
  final double _calMax = 100;
  double _calValue = 0;

  final double _gMin = 0;
  final double _gMax = 999;
  double _gValue = 100;

  final double _kgMin = 0;
  final double _kgMax = 100;
  double _kgValue = 20;
  Color _gTextColor = Colors.green;

  final double _mlMin = 0;
  final double _mlMax = 999;
  double _mlValue = 530;

  final double _lMin = 0;
  final double _lMax = 100;
  double _lValue = 70;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SleekCircularSlider(
              appearance: CircularSliderAppearance(
                customWidths: CustomSliderWidths(progressBarWidth: 10),
              ),
              min: _min,
              max: _max,
              initialValue: _sliderValue,
              onChange: (double newValue) {
                setState(() {
                  _sliderValue = newValue;
                });
              },
            ),
            SleekCircularSlider(
              appearance: CircularSliderAppearance(
                angleRange: 360,
                customWidths: CustomSliderWidths(progressBarWidth: 10),
                customColors: CustomSliderColors(
                  trackColor: Colors.blue.shade100,
                  dotColor: Colors.white,
                  progressBarColor: Colors.blue,
                ),
                infoProperties: InfoProperties(
                  mainLabelStyle: const TextStyle(
                    color: Colors.blue,
                    fontSize: 40.0,
                  ),
                  modifier: (double a) {
                    return _calValue.toStringAsFixed(0);
                  },
                  bottomLabelText: 'kCal',
                  bottomLabelStyle: const TextStyle(
                    color: Colors.blue,
                    fontSize: 12.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              min: _calMin,
              max: _calMax,
              initialValue: _calValue,
              onChange: (double newValue) {
                setState(() {
                  _calValue = newValue;
                });
              },
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(top: 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SleekCircularSlider(
                appearance: CircularSliderAppearance(
                  customWidths: CustomSliderWidths(progressBarWidth: 10),
                  customColors: CustomSliderColors(
                    trackColors: [
                      Colors.red.shade100,
                      Colors.blue.shade100,
                      Colors.green.shade100,
                    ],
                    dotColor: Colors.white,
                    progressBarColors: [
                      Colors.red,
                      Colors.blue,
                      Colors.green,
                    ],
                  ),
                  infoProperties: InfoProperties(
                    topLabelText: 'g',
                    topLabelStyle: TextStyle(
                      fontSize: 16.0,
                      color: _gTextColor,
                    ),
                    mainLabelStyle: TextStyle(
                      color: _gTextColor,
                      fontSize: 30.0,
                    ),
                    modifier: (double a) {
                      return _gValue.toStringAsFixed(0);
                    },
                  ),
                ),
                min: _gMin,
                max: _gMax,
                initialValue: _gValue,
                onChange: (double newValue) {
                  setState(() {
                    _gValue = newValue;
                    if (newValue > 720) {
                      _gTextColor = Colors.red;
                    } else if (newValue > 320) {
                      _gTextColor = Colors.blue;
                    } else {
                      _gTextColor = Colors.green;
                    }
                  });
                },
              ),
              SleekCircularSlider(
                appearance: CircularSliderAppearance(
                  customWidths: CustomSliderWidths(progressBarWidth: 10),
                  customColors: CustomSliderColors(
                    trackColors: [
                      Colors.deepOrange.shade100,
                      Colors.orange.shade100,
                      Colors.orangeAccent.shade100,
                    ],
                    dotColor: Colors.white,
                    progressBarColors: [
                      Colors.deepOrange,
                      Colors.orange,
                      Colors.orangeAccent,
                    ],
                  ),
                  infoProperties: InfoProperties(
                    mainLabelStyle: const TextStyle(
                      color: Colors.orangeAccent,
                      fontSize: 30.0,
                    ),
                    modifier: (double a) {
                      return _kgValue.toStringAsFixed(0) + ' kg';
                    },
                  ),
                ),
                min: _kgMin,
                max: _kgMax,
                initialValue: _kgValue,
                onChange: (double newValue) {
                  setState(() {
                    _kgValue = newValue;
                  });
                },
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SleekCircularSlider(
                appearance: CircularSliderAppearance(
                  customWidths: CustomSliderWidths(
                    progressBarWidth: 10,
                    handlerSize: 2,
                    shadowWidth: 15,
                    trackWidth: 6.0,
                  ),
                  customColors: CustomSliderColors(
                    trackColor: Colors.blueGrey,
                    dotColor: Colors.black,
                    progressBarColors: [
                      Colors.white,
                      Colors.grey,
                      Colors.white,
                    ],
                  ),
                  infoProperties: InfoProperties(
                    mainLabelStyle: TextStyle(
                      fontSize: 30.0,
                      foreground: Paint()
                        ..style = PaintingStyle.stroke
                        ..strokeWidth = 1.5
                        ..color = Colors.grey,
                    ),
                    modifier: (double a) {
                      return _mlValue.toStringAsFixed(0) + ' ml';
                    },
                  ),
                ),
                min: _mlMin,
                max: _mlMax,
                initialValue: _mlValue,
                onChange: (double newValue) {
                  setState(() {
                    _mlValue = newValue;
                  });
                },
              ),
              SleekCircularSlider(
                appearance: CircularSliderAppearance(
                  customWidths: CustomSliderWidths(
                    progressBarWidth: 10,
                    handlerSize: 2.0,
                    shadowWidth: 15.0,
                    trackWidth: 6.0,
                  ),
                  customColors: CustomSliderColors(
                    trackColor: Colors.purple.shade200,
                    dotColor: Colors.black,
                    progressBarColors: [
                      Colors.purple.shade100,
                      Colors.white,
                      Colors.purple.shade100,
                    ],
                  ),
                  infoProperties: InfoProperties(
                    mainLabelStyle: TextStyle(
                      fontSize: 30.0,
                      color: Colors.purple.shade700,
                      fontWeight: FontWeight.bold,
                    ),
                    bottomLabelText: 'liter',
                    bottomLabelStyle: TextStyle(
                      fontSize: 16.0,
                      color: Colors.purple.shade700,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 3.0,
                    ),
                    modifier: (double a) {
                      return _lValue.toStringAsFixed(0);
                    },
                  ),
                ),
                min: _lMin,
                max: _lMax,
                initialValue: _lValue,
                onChange: (double newValue) {
                  setState(() {
                    _lValue = newValue;
                  });
                },
              ),
            ],
          ),
        ),
      ],
    );
  }
}
